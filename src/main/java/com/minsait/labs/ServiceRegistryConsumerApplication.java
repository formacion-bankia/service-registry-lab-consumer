package com.minsait.labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
public class ServiceRegistryConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRegistryConsumerApplication.class, args);
	}

}
